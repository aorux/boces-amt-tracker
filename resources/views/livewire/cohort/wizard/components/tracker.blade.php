<div class="wizard-steps">
    <div class="wizard-step {{ $currentStep != 1 ? '': 'wizard-step-active' }}">
        <div class="wizard-step-icon">
            <i class="fal fa-chart-network"></i>
        </div>
        <div class="wizard-step-label">
            Create Cohort
        </div>
    </div>
    <div class="wizard-step {{ $currentStep != 2 ? '': 'wizard-step-active' }}">
        <div class="wizard-step-icon">
            <i class="fal fa-books"></i>
        </div>
        <div class="wizard-step-label">
            Assign Courses
        </div>
    </div>
    <div class="wizard-step {{ $currentStep != 3 ? '': 'wizard-step-active' }}">
        <div class="wizard-step-icon">
            <i class="fal fa-users"></i>
        </div>
        <div class="wizard-step-label">
            Assign / Import Users
        </div>
    </div>
</div>
