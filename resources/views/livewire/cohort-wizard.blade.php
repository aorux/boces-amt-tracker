
@if (session()->has('message'))
    <div class="alert alert-success">
        {{ session('message') }}
    </div>
@endif

<div class="wizard-steps">
    <div class="wizard-step wizard-step-active">


        <div class="wizard-step-icon">
            <i class="fal fa-chart-network"></i>
        </div>
        <div class="wizard-step-label">
            Create Cohort
        </div>
    </div>
    <div class="wizard-step wizard-step-active">
        <div class="wizard-step-icon">
            <i class="fal fa-books"></i>
        </div>
        <div class="wizard-step-label">
            Assign Courses
        </div>
    </div>
    <div class="wizard-step wizard-step-active">
        <div class="wizard-step-icon">
            <i class="fal fa-users"></i>
        </div>
        <div class="wizard-step-label">
            Assign / Import Users
        </div>
    </div>
</div>
<div class="wizard-pane">

    <form wire:submit.prevent="submit">

        <div>
            @if($step == 0)
                <div class="form-group row align-items-center">
                    <label class="text-md-right text-left">Cohort Name</label>
                    <input  wire:model="name" type="text" class="form-control">
                    @error('name') <span class="error">{{ $message }}</span> @enderror
                </div>

                <div class="form-group row align-items-center">
                    <label class="text-md-right text-left">Start Date</label>
                    <input type="date" wire:model="start_date" class="form-control" id="start_date">
                    @error('start_date') <span class="error">{{ $message }}</span> @enderror
                </div>

                <div class="form-group row align-items-center">
                    <label class="text-md-right text-left">End Date</label>
                    <input type="date" wire:model="end_date" class="form-control" id="end_date">
                    @error('end_date') <span class="error">{{ $message }}</span> @enderror
                </div>
            @endif

            @if($step == 1)
                Courses Available
            @endif

            @if($step == 2)
                Users Available
            @endif

            @if($step > 2)
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Thank you for information</h4>
                        <p class="card-text">Cohort {{ $this->name }} launched successfully.</p>
                        <a href="/">Go to home</a>
                    </div>


                </div>

            @endif
        </div>

        <div class="mt-2">

            @if($step> 0 && $step<=2)
                <button type="button" wire:click="decreaseStep" class="btn btn-secondary mr-3">Back</button>
            @endif


            @if($step <= 2)
                <button type="submit" class="btn btn-success">Next</button>
            @endif

        </div>

    </form>
</div>
