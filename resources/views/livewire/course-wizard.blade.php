<div>
    <div>

        <h3>Course Details</h3>

        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif


        <form wire:submit.prevent="submit">

            <div>


                @if($step == 0)
                    Create The Course
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" wire:model.lazy="name" placeholder="Name">

                        @error('name')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="date" class="form-control" wire:model.lazy="start_date" placeholder="Start Date">
                        @error('email')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="date" class="form-control" wire:model.lazy="end_date" placeholder="End Date">
                        @error('phone_number')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="status">Active?</label>
                        <input type="text" class="form-control" wire:model.lazy="status" placeholder="0 for no 1 for yes">
                        @error('phone_number')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                @endif

                @if($step ==1)
                    // Assign Cohort
                    <div class="form-group">
                        <label for=boces_id">BOCES ID</label>
                        <input type="text" class="form-control" wire:model.lazy="boces_id" placeholder="BOCES ID">
                        @error('boces_id')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="gafe_email">GAFE / Internal Email</label>
                        <input type="email" class="form-control" wire:model.lazy="gafe_email" placeholder="Internal Email">
                        @error('gafe_email')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="administrator_level">Administrator Level</label>
                        <input type="number" class="form-control" wire:model.lazy="administrator_level" placeholder="4 being admin 0 being user">
                        @error('administrator_level')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>
                @endif


                @if($step==2)
                    Assign Instructors & Users
                    <div class="form-group">
                        <label for=cohort_id">COHORT ID</label>
                        <input type="text" class="form-control" wire:model.lazy="cohort_id" placeholder="COHORT ID">
                        @error('cohort_id')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="status_id">Account Status</label>
                        <input type="number" class="form-control" wire:model.lazy="status_id" placeholder="0 inactive 1 active 2 suspended">
                        @error('status_id')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>
                @endif

                @if($step > 2)
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Thank you for information</h4>
                            <p class="card-text">User {{$this->course->name}} creation successful.</p>
                            <a href="/">Go to home</a>
                        </div>


                    </div>

                @endif
            </div>

            <div class="mt-2">

                @if($step> 0 && $step<=2)
                    <button type="button" wire:click="decreaseStep" class="btn btn-secondary mr-3">Back</button>
                @endif


                @if($step <= 2)
                    <button type="submit" class="btn btn-success">Next</button>
                @endif

            </div>

        </form>
    </div>

    {{-- Nothing in the world is as soft and yielding as water. --}}
</div>
