<div>
    <div>

        <h3>User Details</h3>

        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif


        <form wire:submit.prevent="submit">

            <div>


                @if($step == 0)
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" wire:model.lazy="name" placeholder="Name">

                        @error('name')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" wire:model.lazy="email" placeholder="email">
                        @error('email')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="phone_number">Phone Number</label>
                        <input type="text" class="form-control" wire:model.lazy="phone_number" placeholder="Phone Number">
                        @error('phone_number')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                @endif

                @if($step ==1)
                    <div class="form-group">
                        <label for=boces_id">BOCES ID</label>
                        <input type="text" class="form-control" wire:model.lazy="boces_id" placeholder="BOCES ID">
                        @error('boces_id')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="gafe_email">GAFE / Internal Email</label>
                        <input type="email" class="form-control" wire:model.lazy="gafe_email" placeholder="Internal Email">
                        @error('gafe_email')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>

                    <div class="form-group">
                        <label for="administrator_level">Administrator Level</label>
                        <input type="number" class="form-control" wire:model.lazy="administrator_level" placeholder="4 being admin 0 being user">
                        @error('administrator_level')<small class="form-text text-danger">{{ $message }}</small>@enderror
                    </div>
                @endif


                @if($step==2)
                        <div class="form-group">
                            <label for=cohort_id">COHORT ID</label>
                            <input type="text" class="form-control" wire:model.lazy="cohort_id" placeholder="COHORT ID">
                            @error('cohort_id')<small class="form-text text-danger">{{ $message }}</small>@enderror
                        </div>

                        <div class="form-group">
                            <label for="status_id">Account Status</label>
                            <input type="number" class="form-control" wire:model.lazy="status_id" placeholder="0 inactive 1 active 2 suspended">
                            @error('status_id')<small class="form-text text-danger">{{ $message }}</small>@enderror
                        </div>
                @endif

                @if($step > 2)
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Thank you for information</h4>
                            <p class="card-text">User {{$this->user->name}} creation successful.</p>
                            <a href="/">Go to home</a>
                        </div>


                    </div>

                @endif
            </div>

            <div class="mt-2">

                @if($step> 0 && $step<=2)
                    <button type="button" wire:click="decreaseStep" class="btn btn-secondary mr-3">Back</button>
                @endif


                @if($step <= 2)
                    <button type="submit" class="btn btn-success">Next</button>
                @endif

            </div>

        </form>
    </div>

    {{-- Nothing in the world is as soft and yielding as water. --}}
</div>
