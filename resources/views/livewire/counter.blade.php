<div>
    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
    <div style="text-align: center">
        <button wire:click="increment">+</button>
        <h1>{{ $count }}</h1>
        <h1>{{ $name }}</h1>
        <input type="text" wire:model="name">

    </div>
</div>
