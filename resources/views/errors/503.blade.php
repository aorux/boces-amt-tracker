@extends('layouts.app')

@section('title', 'Layanan Tidak Tersedia')
@section('code', '503')

@section('content')
<div class="page-error mt-5">
  <div class="page-inner">
    <h1>503</h1>
    <div class="page-description">
      Down for Maintenance
    </div>
    <div class="page-search">
      <div class="mt-2">
        <a href="{{ route('welcome') }}">Return Home</a>
      </div>
    </div>
  </div>
</div>
@endsection
