@extends('layouts.app')

@section('title', 'Server Bermasalah')
@section('code', '500')

@section('content')
<div class="page-error mt-5">
  <div class="page-inner">
    <h1>500</h1>
    <div class="page-description">
      Internal Server Error
    </div>
    <div class="page-search">
      <div class="mt-2">
        <a href="{{ route('welcome') }}">Return Home</a>
      </div>
    </div>
  </div>
</div>
@endsection
