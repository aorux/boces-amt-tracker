@extends('layouts.backend')

@section('title', 'Index')

@section('content')
    <div class="section-header">
        <h1>{{ $cohort->name }} | Cohort Overview</h1>
    </div>
    <div class="section-body">
        <div class="section">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-statistic-2">
                        <div class="card-stats">
                            <div class="card-stats-title">Quick Statistics</div>

                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label">Start Date</div>
                                    <div class="card-stats-item-count">{{ $cohort->start_date }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label">End Date</div>
                                    <div class="card-stats-item-count">{{ $cohort->end_date }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label">Status</div>
                                    <div class="card-stats-item-count">{{ $cohort->status_id }}</div>
                                </div>
                            </div>
                        </div>

                        <div class="card-icon shadow-primary bg-primary">
                            <i class="fas fa-users"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Students Enrolled</h4>
                            </div>
                            <div class="card-body mb-3">
                                {{ $cohort->student_count }}
                            </div>
                            <div class="card-footer bg-whitesmoke text-right">
                                Footer Card
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Courses</h4>
                            <div class="card-header-form">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Name</th>
                                        <th>Number</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach ($cohort->courses as $course)
                                        <tr>
                                            <td>{{ $course->name }}</td>
                                            <td>{{ $course->course_number }}</td>
                                            <td>
                                                @include('shorthand.StatusSwitch', [ $status_id = $course->status_id ])
                                            </td>
                                            <td><a href="#" class="btn btn-secondary">Edit</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Accounts Associated</h4>
                            <div class="card-header-form">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Avatar</th>
                                        <th>BOCES ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach($cohort->users as $user)
                                        <tr>
                                            <td>
                                                <img alt="image" src="{{ $user->avatar_url }}" class="rounded-circle" width="35" data-toggle="tooltip" title="Wildan Ahdian">
                                            </td>
                                            <td>{{ $user->boces_id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone_number }}</td>
                                            <td>{{ $user->administrator_level }}</td>
                                            <td>
                                                @include('shorthand.StatusSwitch', [ $status_id = $user->status_id ])
                                            </td>
                                            <td><a href="#" class="btn btn-secondary">Edit</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <livewire:counter />
</div>
@endsection
