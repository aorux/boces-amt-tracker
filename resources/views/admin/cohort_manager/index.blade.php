@extends('layouts.backend')

@section('title', 'Index')

@section('content')
    <div class="section-header">
        <h1>Cohort Manager</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="col-3">
                            <h4>Cohort Manager</h4>
                        </div>
                        <div class="col-6">
                            <div class="card-header-form">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-3">
                            <a href="#" class="btn btn-icon icon-left btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fal fa-plus"></i> Create New Cohort</a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Name</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Enrollment Count</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($cohorts as $cohort)
                                    <tr>
                                        <td>{{ $cohort->name }}</td>
                                        <td>{{ $cohort->start_date }}</td>
                                        <td>{{ $cohort->end_date }}</td>
                                        <td>
                                            @include('shorthand.StatusSwitch', [ $status_id = $cohort->status_id ])
                                        </td>
                                        <td>1</td>
                                        <td><a href="{{ route('cohorts.show', $cohort->id) }}" class="btn btn-secondary">Edit</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('modals')
    <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create New Cohort</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
