@extends('layouts.backend')

@section('title', 'Index')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Create</h4>
    </div>

    <div class="card-body">
        <form action="{{ route('cohorts.store') }}" method="post">
            @csrf
            Name
            <input name="name" type="text">
            Status
            <input name="status" type="text">
            Start Date
            <input name="start_date" type="text">
            End Date
            <input name="end_date" type="text">

            <input type="submit">
        </form>

        Creation Forms
        '' 1 Create Cohort
        '' 4 Associate Courses
        '' 4 Import Users
        '' 5 Activate Cohort
        '' 6 Administer Cohort
    </div>
    <livewire:counter />
</div>
@endsection
