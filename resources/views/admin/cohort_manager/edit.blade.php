@extends('layouts.backend')

@section('title', 'Index')

@section('content')
<div class="card">
    <div class="card-header">
        <h4>Edit</h4>
    </div>

    <div class="card-body">
            {{ $cohort->name }}
            {{ $cohort->start_date }}
            {{ $cohort->end_date }}
            {{ $cohort->status_id }}
            {{ $cohort->modification_lock }}
    </div>

    <livewire:counter />
</div>
@endsection
