@extends('layouts.backend')

@section('title', 'User Creation Wizard')

@section('content')
    <div class="section-header">
        <h1>User Creation Wizard</h1>
    </div>

    First Name
    Last Name
    Birthday
    BOCES ID / GAFE
    EMAIL
    Password

    Account Status
    Administrator ?
    Cohort (if applicable)
    Courses (if applicable)
    Module Record (if applicable)

    <livewire:user-wizard />
    <livewire:counter />

@endsection
