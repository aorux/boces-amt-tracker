@extends('layouts.backend')

@section('title', 'Course Manager')

@section('content')
    <div class="section-header">
        <h1>Course Creation Wizard</h1>
    </div>

    Course Name
    Course Modules
    Course Users
    Schedule
    Attendance

    <livewire:course-wizard />
    <livewire:counter />

@endsection
