@extends('layouts.backend')

@section('title', 'Course Manager')

@section('content')
    <div class="section-header">
        <h1>Cohort Creation Wizard</h1>
    </div>

    Cohort Name
    Status
    Start Date
    End Date

    <livewire:cohort-wizard />
    <livewire:counter />
@endsection
