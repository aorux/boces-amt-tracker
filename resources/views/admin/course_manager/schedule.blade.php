@extends('layouts.backend')

@section('title', 'Index')

@section('content')
    <div class="section-header">
        <h1>{{ $course->name }} | Course Schedule</h1>
    </div>
    <div class="section-body">
        <div class="section">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card card-statistic-2">
                        <div class="card-stats">
                            <div class="card-stats-title">Course Statistics</div>
                            <div class="card-stats-items">
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label">Start Date</div>
                                    <div class="card-stats-item-count">{{ $course->start_date->format('m/d/Y') }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label">End Date</div>
                                    <div class="card-stats-item-count">{{ $course->end_date->format('m/d/Y') }}</div>
                                </div>
                                <div class="card-stats-item">
                                    <div class="card-stats-item-label">Status</div>
                                    <div class="card-stats-item-count">
                                        @include('shorthand.StatusSwitch', [ $status_id = $course->status_id ])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-icon shadow-primary bg-primary">
                            <i class="fas fa-users"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Total Course Modules</h4>
                            </div>
                            <div class="card-body mb-3">
                                21
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Course Info</h4>
                        </div>
                        <table class="table table-striped mb-0">
                            <tbody>
                            <tr>
                                <th>Instructor</th>
                                <td>Giovanni Medrano</td>
                            </tr>
                            <tr>
                                <th>Course Number</th>
                                <td>{{ $course->course_number }}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>General</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Associated Cohort</h4>
                        </div>
                        <table class="table table-striped mb-0">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <td>{{ $course->cohort->name }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>@include('shorthand.StatusSwitch', [ $status_id = $course->cohort->status_id ])</td>
                            </tr>
                            <tr>
                                <th>Modification Lock</th>
                                <td>{{ $course->cohort->modification_lock }}</td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>{{ $course->cohort->start_date }}</td>
                            </tr>
                            <tr>
                                <th>End Date</th>
                                <td>{{ $course->cohort->end_date }}</td>
                            </tr>
                            <tr>
                                <th>Student Count</th>
                                <td>{{ $course->cohort->student_count }}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Accounts Associated</h4>
                            <div class="card-header-form">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tr>
                                        <th>Avatar</th>
                                        <th>BOCES ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone Number</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>

                                    @foreach($course->users as $user)
                                        <tr>
                                            <td>
                                                <img alt="image" src="{{ $user->avatar_url }}" class="rounded-circle" width="35" data-toggle="tooltip" title="Wildan Ahdian">
                                            </td>
                                            <td>{{ $user->boces_id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->phone_number }}</td>
                                            <td>{{ $user->administrator_level }}</td>
                                            <td>
                                                @include('shorthand.StatusSwitch', [ $status_id = $user->status_id ])
                                            </td>
                                            <td><a href="#" class="btn btn-secondary">Edit</a></td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                Manage Course Schedule
                Manage Course Attendance
            </div>
        </div>
    </div> 

    <livewire:counter />
    </div>
@endsection
