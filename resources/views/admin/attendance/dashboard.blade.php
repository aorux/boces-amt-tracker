@extends('layouts.backend')

@section('title', 'Attendance')

@section('content')


    <div class="card">
        <div class="card-header">
            <h4>Attendance Dashboard</h4>
        </div>

        <div class="card-body">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Explicabo ducimus temporibus aut ratione maxime
            accusamus
            commodi esse itaque tenetur. Temporibus ducimus ea veniam quisquam rerum vitae? Animi quos laudantium quam?
        </div>

        <livewire:counter />
    </div>
@endsection
