@extends('layouts.backend')

@section('title', 'Dashboard')

@section('content')

<div class="row">
    <div class="col-5">
        <div class="card">
            <div class="card-header">
                <h4>Your Sections</h4>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <tr>
                            <th>#</th>
                            <th>Course Name</th>
                            <th>Student Count</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Airframe</td>
                            <td>25</td>
                            <td><div class="badge badge-success">Active</div></td>
                            <td><a href="#" class="btn btn-secondary">Details</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header">
                <h4>Lessons in Progress</h4>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <tr>
                            <th>#</th>
                            <th>Course Name</th>
                            <th>Lesson / Day</th>
                            <th>Status</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Airframe</td>
                            <td>Aerodynamics</td>
                            <td><a href="#" class="btn btn-secondary">Details</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-7">
        <livewire:calendar />
    </div>
</div>

<div class="row">
</div>

    <livewire:counter />
</div>
@endsection

