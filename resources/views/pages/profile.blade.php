@extends('layouts.backend')

@section('title', 'Profile')

@section('content')
<x-section-header heading="Profile" breadcrumb="profile" />
<x-section-sub-header title="Hi {{ Str::words(auth()->user()->name, 1, '') }}!"
  lead="Change information about yourself on this page." />

<div class="row mt-sm-4">
  <div class="col-12 col-md-12 col-lg-12">
    @if (session('status') or Auth::user()->email_verified_at == null)
    <div class="alert alert-primary alert-dismissible show fade">
      <div class="alert-body">
        <button class="close" data-dismiss="alert">
          <span>&times;</span>
        </button>
        @if (session('status')=='profile-information-updated')
        Profile has been updated.
        @endif
        @if (session('status')=='password-updated')
        Password has been updated.
        @endif
        @if (session('status')=='two-factor-authentication-disabled')
        Two factor authentication disabled.
        @endif
        @if (session('status')=='two-factor-authentication-enabled')
        Two factor authentication enabled.
        @endif
        @if (session('status')=='recovery-codes-generated')
        Recovery codes generated.
        @endif
        @if (Auth::user()->email_verified_at == null)
          Please verify your email.
        @endif
      </div>
    </div>
    @endif

    @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updateProfileInformation()))
        @livewire('profile.update-profile-information-form')
    @endif

    @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
        @livewire('profile.update-password-form')
    @endif

    @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::twoFactorAuthentication()))
        @livewire('profile.two-factor-authentication-form')
    @endif
  </div>
</div>
@endsection
