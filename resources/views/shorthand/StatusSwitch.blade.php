@switch($status_id)
    @case(1)
        <div class="badge badge-dark">Unset</div>
    @break
    @case(2)
        <div class="badge badge-success">Active</div>
    @break
    @case(3)
        <div class="badge badge-secondary">Inactive</div>
    @break
    @case(4)
        <div class="badge badge-danger">Locked</div>
    @break
    @case(5)
        <div class="badge badge-success">Unlocked</div>
    @break
    @case(6)
        <div class="badge badge-danger">Suspended</div>
    @break
    @case(7)
        <div class="badge badge-info">In Progress</div>
    @break
    @case(8)
    <div class="badge badge-warning">Pending</div>
    @break
    @default
        <div class="badge badge-success">Unset</div>
@endswitch
