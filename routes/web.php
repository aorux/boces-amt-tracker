<?php

use App\Http\Controllers\Admin\CohortController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'welcome'])->name('welcome');

Route::middleware(['auth:sanctum', 'verified'])->group( function () {
    Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
    Route::get('/profile', [HomeController::class, 'profile'])->name('profile');

    // Admin Routes
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/', [DashboardController::class, 'dashboard'])->name('admin.dashboard');
        Route::resource('users', UserController::class);
        Route::resource('cohorts', CohortController::class);
        Route::resource('courses', CourseController::class);

        Route::resource('calendar', CalendarController::class);

        Route::prefix('attendance')->group(function () {
            Route::get('/', [DashboardController::class, 'dashboard'])->name('attendance_dashboard');
        });

        Route::group(['prefix' => 'wizard'], function() {
            Route::get('user', [\App\Http\Controllers\Admin\WizardController::class , 'userCreationWizard'])->name('user_wizard');
            Route::get('cohort', [\App\Http\Controllers\Admin\WizardController::class , 'cohortCreationWizard'])->name('cohort_wizard');
            Route::get('course', [\App\Http\Controllers\Admin\WizardController::class , 'courseCreationWizard'])->name('course_wizard');
        });
    });

    Route::get('testing', function() {
        return Auth::user()->profile_photo_url;
    });

});
