<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('section');
            $table->string('course_number');
            $table->unsignedBigInteger('cohort_id');
            $table->string('course_category_id');
            $table->unsignedBigInteger('instructor_id');
            $table->unsignedBigInteger('status_id');
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->unsignedBigInteger('verification_admin');
            $table->unsignedBigInteger('verification_admin_auditor');
            $table->integer('modification_lock');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course');
    }
}
