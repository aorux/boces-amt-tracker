<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cohort;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        //$user = User::findOrFail(1);
        //$user->cohort()->associate(1);
        //user->save();

        $cohort = Cohort::findOrFail(1);
        return $cohort;

    }
}
