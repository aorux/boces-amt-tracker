<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cohort;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;

class WizardController extends Controller
{
    // User Creation Wizard
    public function userCreationWizard()
    {
        $cohorts = Cohort::all();
        $courses = Course::all();
        // Schedule = Schedule::all();
        return view('admin.wizards.UserCreationWizard', compact('cohorts', 'courses'));
    }

    /// Course Creation Wizard
    public function courseCreationWizard()
    {
        $cohorts = Cohort::all();
        $users = User::all();
        // Schedule = Schedule::all();
        return view('admin.wizards.CourseCreationWizard', compact('cohorts', 'users'));
    }

    /// Cohort Creaion Wizard
    public function cohortCreationWizard()
    {
        $users = User::all();
        $courses = Course::all();
        // Schedule = Schedule::all();
        return view('admin.wizards.CohortCreationWizard', compact('courses', 'users'));
    }
}
