<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cohort;
use Illuminate\Http\Request;
use View;

class CohortController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */


    public function index()
    {
        $cohorts = Cohort::all();
        return View::make('admin.cohort_manager.index', compact('cohorts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return View::make('admin.cohort_manager.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $cohort = new Cohort();
        $cohort->name = $request->name;
        $cohort->status_id = $request->status;
        $cohort->start_date = $request->start_date;
        $cohort->end_date = $request->end_date;
        $cohort->modification_lock = 0;
        $cohort->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $cohort = Cohort::with('courses')->findOrFail($id)->makeVisible('users');
        return View::make('admin.cohort_manager.show', compact('cohort'));
        // dd($cohort);
        //return $cohort;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $cohort = Cohort::findOrFail($id);
        return View::make('admin.cohort_manager.edit', compact('cohort'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        Cohort::findOrFail($id)->update($request->all());
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //return 'hi';
        //return Cohort::findOrFail($id)->delete();
    }
}
