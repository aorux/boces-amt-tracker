<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Calendar[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return Calendar::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $calendar = Calendar::create($request->all());

        return response()->json($calendar, 201);
    }


    /**
     * Display the specified resource.
     *
     * @param Calendar $calendar
     * @return Calendar
     */
    public function show(Calendar $calendar)
    {
        return $calendar;
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Calendar $calendar
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Calendar $calendar)
    {
        $calendar->update($request->all());

        return response()->json($calendar, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Calendar $calendar
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Calendar $calendar)
    {
        $calendar->delete();

        return response()->json(null, 204);
    }
}
