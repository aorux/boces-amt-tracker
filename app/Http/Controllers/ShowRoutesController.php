<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShowRoutesController extends Controller
{
    public function index(){
        $app = app();
        $routes = $app->routes->getRoutes();
        return view ('routes',compact('routes'));
    }
}
