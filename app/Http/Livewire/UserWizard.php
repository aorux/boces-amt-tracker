<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Livewire\Component;

class UserWizard extends Component
{
    public $name;
    public $boces_id;
    public $email;
    public $gafe_email;
    public $cohort_id;
    public $status_id;
    public $phone_number;
    public $administrator_level;

    public $step;

    public $user;

    private $stepActions = [
        'step1',
        'step2',
        'step3',
    ];

    public function mount()
    {
        $this->step = 0;
    }

    public function decreaseStep()
    {
        $this->step--;
    }

    public function submit()
    {

        $action = $this->stepActions[$this->step];

        $this->$action();
    }

    // Step 1
    public function step1()
    {
        $this->validate([
            'name'            => 'required|min:4',
            'email'           => 'email|required',
            'phone_number'    => 'numeric|required',
        ]);

        if ($this->user) {
            $this->user= tap($this->user)->update([
                'name'          => $this->name,
                'email'         => $this->email,
                'phone_number'  => $this->phone_number
            ]);
            session()->flash('message', 'User successfully updated.');

        }else {
            $this->user = User::create([
                'name'              => $this->name,
                'email'             => $this->email,
                'phone_number'      => $this->phone_number,
                'password'          => Hash::make(Str::random(10)),
            ]);
            session()->flash('message', 'User successfully created.');

        }


        $this->step++;
    }

    public function step2()
    {
        $this->validate([
            'boces_id'              => 'string',
            'gafe_email'            => 'email|required',
            'administrator_level'   => 'numeric|required'
        ]);

        $this->user = tap($this->user)->update([
            'boces_id' => $this->boces_id,
            'gafe_email' => $this->gafe_email,
            'administrator_level' => $this->administrator_level
        ]);

        $this->step++;
    }

    public function step3()
    {
        $this->validate([
            'cohort_id'             => 'numeric',
            'status_id'             => 'numeric|required',
        ]);

        $this->user = tap($this->user)->update([
            'cohort_id'    => $this->cohort_id,
            'status_id'    => $this->status_id
        ]);

        session()->flash('message', 'user registration complete');

        $this->step++;
    }

    public function render()
    {
        return view('livewire.user-wizard');
    }
}
