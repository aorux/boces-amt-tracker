<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CourseWizard extends Component
{
    public $name;
    public $start_date;
    public $end_date;
    public $status;
    public $instructor;
    public $users;

    public $step;

    public $course;

    private $stepActions = [
        'step1',
        'step2',
        'step3',
    ];

    public function mount()
    {
        $this->step = 0;
    }

    public function decreaseStep()
    {
        $this->step--;
    }

    public function submit()
    {

        $action = $this->stepActions[$this->step];

        $this->$action();
    }

    public function render()
    {
        return view('livewire.course-wizard');
    }
}
