<?php

namespace App\Http\Livewire;

use App\Models\Cohort;
use App\Models\User;
use Livewire\Component;

class CohortWizard extends Component
{
    public $name;
    public $start_date;
    public $end_date;
    public $status_id;
    public $modification_lock;

    public $step;

    public $cohort;

    private $stepActions = [
        'step1',
        'step2',
        'step3',
    ];

    public function mount()
    {
        $this->step = 0;
    }

    public function decreaseStep()
    {
        $this->step--;
    }

    public function submit()
    {

        $action = $this->stepActions[$this->step];

        $this->$action();
    }

    // Step 1
    public function step1()
    {
        $this->validate([
            'name'              => 'required|min:4',
            'start_date'        => 'required',
            'end_date'          => 'required',
        ]);


        $this->cohort = Cohort::create([
            'name'                  => $this->name,
            'start_date'            => $this->start_date,
            'end_date'              => $this->end_date,
            'status_id'             => 1,
            'modification_lock'     => 0
        ]);

        return $this->start_date;
        session()->flash('message', 'cohort successfully created.');



        $this->step++;
    }

    public function step2()
    {
        // assign users
        $users = User::where('cohort_id', null)->get();

        $this->step++;
    }

    public function step3()
    {

        $this->step++;
    }

    public function render()
    {
        return view('livewire.cohort-wizard');
    }
}
