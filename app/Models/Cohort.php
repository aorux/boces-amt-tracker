<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Cohort
 *
 * @property int $id
 * @property string $name
 * @property int $status_id
 * @property int $modification_lock
 * @property string $start_date
 * @property string $end_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereModificationLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cohort whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Cohort extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'status_id',
        'modification_lock',
        'start_date',
        'end_date',
    ];

    protected $casts = [
        'completed_at'
    ];

    protected $hidden = [
        'users'
    ];

    protected $appends = [
        'student_count'
    ];

    public function getStudentCountAttribute()
    {
        return count($this->users);
    }

    public function courses() {
        return $this->hasMany(Course::class);
    }

    public function users() {
        return $this->hasMany(User::class);
    }
}
