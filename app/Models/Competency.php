<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Competency
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Competency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Competency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Competency query()
 * @mixin \Eloquent
 */
class Competency extends Model
{
    use HasFactory;


}
