<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Course
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attendance[] $attendance
 * @property-read int|null $attendance_count
 * @property-read \App\Models\Cohort $cohort
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Competency[] $competencies
 * @property-read int|null $competencies_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $courses
 * @property-read int|null $courses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Course newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Course query()
 * @mixin \Eloquent
 */
class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_name',
        'course_number',
        'course_category_id',
        'cohort_id',
        'instructor_id',
        'status_id',
        'start_date',
        'end_date',
        'completed_at',
        'verification_admin_id',
        'verification_admin_audit_id',
        'modification_lock',
    ];

    protected $casts = [
        'start_date' => 'datetime: m/d/Y',
        'end_date' => 'datetime: m/d/Y',
        'completed_at' => 'datetime: m/d/Y'
    ];

    public function cohort()
    {
        return $this->belongsTo(Cohort::class);
    }

    public function users()
    {
        return $this->hasManyThrough(
            User::class, Course::class,
            'cohort_id', 'cohort_id',
            'id','id'
        );
    }

    /*
    public function attendance()
    {
        return $this->belongsToMany(Attendance::class);
    }

    public function competencies()
    {
        return $this->hasMany(Competency::class);
    }
    */
}
